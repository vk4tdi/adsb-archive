# ADS-B Flight Tracking

I wanted to log the aircraft activity around my place so I could get a feel for where the air routes are, especially after a second runway was opened up at YBBN Brisbane. This has changed the paths and now there's a lot more heavy jet traffic overhead. Rather than rely on third party or goverment sites I thought I'd store my data.

## Raspberry Pi and RTL-SDR

My ADS-B receiver is a RTL-SDR (like a DVB TV tuner, but built properly) connected to a Raspberry Pi 2+. The Pi is running DUMP1090 to do the decoding, and the data is fed off to a number of tracking websites (ADS-B Exchange, OpenSky, FlightRadar24, RadarBox, ADSBHub).

## Collecting data

The inspiration was [dump1090-stream-parsery](https://github.com/yanofsky/dump1090-stream-parser/blob/master/dump1090-stream-parser.py) by [David Yanofsky]https://github.com/yanofsky). It stores all the ADS-B messages into an SQLite DB.

I've made a few changes:
* Use MariaDB as the data store, pretty trivial change
* Processing and blending of messages to have single entry with ICAO ID, Registration, Lat, Lon, Alt, Heading, Speed, Vert Speed.
* Try and store entries for each aircraft roughly every 300 m, using the speed report to set interval.
* Connection settings stored in a YAML file.
* Python version changed from v2 to v3, which was needed for MariaDB support.

I'm more used to processing data using R rather than Python, so I've created some R routines to grab and process data.
* Generate a list of all 14 bit ICAO ID prefixes a spreadsheet that reproduces ICAO Annex 10 - Aeronautical Communications, Volume III, Chapter 9, Table 9-1
* Import Australian aircraft registration, OpenSky aircraft details, ADSBExchange aircraft detials, ICAO Doc 8643 type & manufacturer info (from OpenSky)
* An example of queries of latest reports for each unique aircraft, looking up country and aircraft details from the database
* Convert all the recorded points into a [GeoPackage](https://www.geopackage.org/) dataset for an area of interest, to be visualised using tools like R or QGIS.

## Usage

Here's a rough guide for using the scripts/tools here.

1. Create a database. 
  + If you use MariaDB then I suggest creating a specific user/password just for that database. Daniel López Azaña has a [good guide](https://www.daniloaz.com/en/how-to-create-a-user-in-mysql-mariadb-and-grant-permissions-on-a-specific-database/) on how to do this. 
  + If you use SQLite then the libraries will have to be changed in the .py and .R files, but no big deal.
1. Copy **db_credentials.yaml.sample** to **db_credentials.yaml** and edit the settings to suit your install. 
  + The database server doesn't have to be the same machine as the ADSB receiver. I use as NAS as my database host.
1. Edit the R files so the command that sets the working directory reflects where the script is.
  + This is a shortcoming of R, in that it isn't easy to know exactly where a script is. This makes locating the YAML file tricky.
  + There are some ways of working around it with RStudio, but I want to be able to run my scripts "as is" using Rscript.
1. Build the reference database with **import_database_info.R**
1. Create the ICAO prefixes with **create_icao24_list.R**
1. Start collecting data by running  **python3 dump1090_process_db.py**
  + Each report stored in the database will be printed.  An example is below (... &.?km for privacy):

		2021-11-07 07:36:51.634000 7C6C9F JST753 -28.98...° 152.52372° 425kt 31075ft 1664ft/min -- 171.?km
		2021-11-07 07:36:51.811000 7C17B0 EYQ -27.47...° 153.18142° 216kt 9050ft -192ft/min -- 19.?km
		2021-11-07 07:36:52.905000 7C19BE FD480 -26.56...° 152.99994° 207kt 22250ft 448ft/min -- 104.?km
		2021-11-07 07:36:53.107000 7C39F6 QLK381D -27.59...° 152.91253° 218kt 5475ft -704ft/min -- 13.?km
		2021-11-07 07:36:53.234000 7C6C9F JST753 -28.98...° 152.52230° 424kt 31125ft 1728ft/min -- 172.?km
		2021-11-07 07:36:53.293000 7C6B0D JST801 -27.49...° 153.04325° 156kt 2475ft -960ft/min -- 5.?km

1. Run a query of the latest observations using **latest_aircraft_query.R**
1. Generate spatial data using **make_spatial_data.R**

## Visualisation

If you want some geographic data to build base maps with in QGIS I suggest looking at [Natural Earth](https://www.naturalearthdata.com/). It's royalty free and has a lot of information for the whole world. I recommend grabbing the [GeoPackage Bundle](http://naciscdn.org/naturalearth/packages/natural_earth_vector.gpkg.zip) because it has everything in one place.

If you live in Queensland then there's a HUGE amount of data at [Queensland Spatial Catalogue](https://qldspatial.information.qld.gov.au/catalogue/custom/index.page). Nation-wide data is available for [Australia](https://data.gov.au/data/dataset/a0650f18-518a-4b99-a553-44f82f28bb5f), [New Zealand](https://data.linz.govt.nz/data/), and [United States](https://www.usgs.gov/products/data-and-tools/gis-data). There are bound to be others out there too.

## 3D Visualisation

I've created line strings from the points using a "unique ID" created from the UTC day, the registration of the aircraft and the callsign. Things might break around 00:00Z, but given my focus is on night flights this isn't a big deal for me. If 00:00Z is in the night whereever you are then try converting to a timezone where 00:00 is the quietest daylight time of day for you, and then extract the date.

QGIS has a 3D view, but it's a bit cumbersome. The [Qgis2threejs](https://github.com/minorua/Qgis2threejs) plugin is fantastic! It can create a self-contained web export. I found that I needed to tick the 'Enable the Viewer to Run Locally' for the 3D objects to work, even on a webserver. The plugin can export a [glTF](https://www.khronos.org/gltf/) file that other viewers (e..g https://gltf-viewer.donmccurdy.com/) can read in. A glTF sample is provided.

## Timeseries Database

The most recent update includes support for [InfluxDB v2.x](https://portal.influxdata.com/downloads/) as a store for _all_ received messages (no filtering to get 300 m spacing). There's no code to use the stored data yet, but the dashboards in InfluxDB are pretty cool. The database looks to be efficient. The code is set up to use the ICAO hex code and callsign as 'tags' and the lat/lng/alt/speed/heading/vert are measurements.

There is R support for [InfluxDB on CRAN](https://cran.r-project.org/web/packages/influxdbclient/index.html), so that will be the next thing to play with.

I recommend configuring the [Influx CLI client](https://docs.influxdata.com/influxdb/cloud/reference/cli/influx/) so backups etc are easy. A query for the number of observations is

		from(bucket: "adsb_obs")
		|> range(start: -365d, stop: now())
		|> group()
		|> count()

11,431,254 records takes up 25 Mbyte in a compressed backup, and when all the .gz files are uncompressed the required space is 43 Mbyte. This data was collected in 3.5 days in Brisbane.