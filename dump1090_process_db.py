#!/usr/bin/env python
# encoding: utf-8

# Code adapted from https://github.com/yanofsky/dump1090-stream-parser/blob/master/dump1090-stream-parser.py

# Sources for other useful info:
# Aircraft, Types, Manufacturers: https://opensky-network.org/datasets/metadata/
# VH Registrations: https://services.casa.gov.au/csv/acrftreg.csv

# Update notes
# 2021-11-13: Added in support for InfluxDB 1.x. Store all measurments (no distance filtering) in this timeseries DB.
# 2021-11-18: Changed how data is extracted from fields. Use all message types where avialable, and store numeric data in dictionaries. Only update when all info is available.

import socket
import sqlite3
import datetime
import mariadb
import time
import sys
import yaml
from haversine import haversine, Unit
import math
from influxdb_client import InfluxDBClient, Point, WritePrecision # This is for InfluxDB2
from influxdb_client.client.write_api import SYNCHRONOUS  # This is for InfluxDB2

with open("db_credentials.yaml", "r") as stream:
    try:
        yaml_info = yaml.safe_load(stream)
        adsb_config = yaml_info["adsb"]
        influx_config = yaml_info["influx"]
        spatial_config = yaml_info["spatial"]
    except yaml.YAMLError as exc:
        print(exc)

#defaults
BUFFER_SIZE = 100
CONNECT_ATTEMPT_LIMIT = 20
CONNECT_ATTEMPT_DELAY = 5.0
UPDATE_DIST = 300.0 # Use speed info to try and get a database entry every 300 metres. About 1s updates for M0.79 737 etc

# Storage dictionarys
callsigns = {}
speeds = {}
headings = {}
vert_rate = {}
latitudes = {}
longitudes = {}
altitudes = {}
updates = {}
squarks = {}
ongrounds = {}

# SBS protoccol fields, based on http://woodair.net/sbs/article/barebones42_socket_data.htm
sbs_fields = {
	'TT': 1, 'Hex': 4,
	'DMG': 6, 'TMG': 7,
	'CS': 10,
	'Alt': 11,
	'GS': 12, 'Trk': 13,
	'Lat': 14, 'Lng': 15,
	'VR': 16,
	'Sq': 17,
	'Gnd': 21
}

# Get the observer's position
my_pos = (spatial_config["MY_LAT"],spatial_config["MY_LON"])
my_alt = float(spatial_config["MY_ALT"])

def main():
	count_since_commit = 0
	count_total = 0
	count_failed_connection_attempts = 1

	# connect to MariaDB database or create if it doesn't exist
	try:
		conn = mariadb.connect(user=adsb_config["DB_USERNAME"], 
		   password=adsb_config["DB_PASSWORD"],  host=adsb_config["DB_HOSTNAME"], port=3306, 
		   database=adsb_config["DB_DATABASE"])
	except mariadb.Error as e:
		print(f"Error connecting to MariaDB Platform: {e}")
		sys.exit(1)

	cur = conn.cursor()
	cur.execute("SET autocommit=1;")

	# set up the table if neccassary
	cur.execute("CREATE TABLE IF NOT EXISTS aircraft_details(datetime_iso TEXT, icao_id TEXT, callsign TEXT,"+
		"lat REAL, lon REAL, altitude INT, ground_speed INT, track INT, vert_rate INT, "+
		"INDEX (icao_id(6)));")
		

	cur.execute("CREATE VIEW IF NOT EXISTS v_latest_obs as select max(datetime_iso) as latest,icao_id,callsign from aircraft_details group by icao_id order by latest desc;")

	# open a socket connection
	while count_failed_connection_attempts < CONNECT_ATTEMPT_LIMIT:
		try:
			s = connect_to_socket(adsb_config["SDR_HOST"], adsb_config["SDR_PORT"])
			count_failed_connection_attempts = 1
			print("Connected to dump1090 broadcast")
			break
		except socket.error:
			count_failed_connection_attempts += 1
			print("Cannot connect to dump1090 broadcast. Making attempt %s." % (count_failed_connection_attempts))
			time.sleep(CONNECT_ATTEMPT_DELAY)
	else:
		conn.commit()
		conn.close()
		quit()

	
	# Connect to the InfluxDB database. Set influx_valid to True if it is good. If it fails, keep using MySQL
	try:
		influxclient = InfluxDBClient(url=influx_config["HOST"], token=influx_config["TOKEN"], org=influx_config["ORG"])
		# influxclient = InfluxDBClient(host=influx_config["DB_HOSTNAME"], port=8086, username=influx_config["DB_USERNAME"], password=influx_config["DB_PASSWORD"])
		# Dummy query to check connection 
		influx_write_api = influxclient.write_api(write_options=SYNCHRONOUS)

	except Exception as e:
		print("Error: %s" % e)
		influx_valid = False
	else:
		influx_valid = True

	data_str = ""
	
	try:
		#loop until an exception
		while True:
			# receive a stream message
			try:
				message = ""
				message = s.recv(BUFFER_SIZE)
				message_decoded = message.decode('utf-8')
				data_str += message_decoded.strip("\n")
			except socket.error:
				# this happens if there is no connection and is delt with below
				pass

			if len(message) == 0:
				print("No broadcast received. Attempting to reconnect")
				time.sleep(CONNECT_ATTEMPT_DELAY)
				s.close()

				while count_failed_connection_attempts < CONNECT_ATTEMPT_LIMIT:
					try:
						s = connect_to_socket(adsb_config["SDR_HOST"], adsb_config["SDR_PORT"])
						count_failed_connection_attempts = 1
						print("Reconnected!")
						break
					except socket.error:
						count_failed_connection_attempts += 1
						print("The attempt failed. Making attempt %s." % (count_failed_connection_attempts))
						time.sleep(CONNECT_ATTEMPT_DELAY)
				else:
					if influx_valid:
						influxclient.close()

					conn.commit()
					conn.close()

					quit()

				continue

			# it is possible that more than one line has been received
			# so split it then loop through the parts and validate

			line_timestamp = datetime.datetime.utcnow()
			data = data_str.split("\n")

			for d in data:
				# now have a row of data from DUMP1090
				line = d.split(",")

				#if the line has 22 items, it's most likely valid
				if len(line) == 22:
					# Slightly different appraoch to original code -- will take info from any message and update
					# Fields 11-22.

					# Always have Hex ID and Message Generated fields
					icao = line[sbs_fields["Hex"]]
					ts_string = line[sbs_fields["DMG"]] + " " + line[sbs_fields["TMG"]]

					# Need to have exception here around time, because it has failed before
					try:
						timestamp = datetime.datetime.strptime(ts_string, "%Y/%m/%d %H:%M:%S.%f")
						sql_ts = datetime.datetime.strftime(timestamp, "%Y-%m-%d %H:%M:%S.%f")
					except Exception as e:
						print("Error: %s" % e)
					else:
						# Carry on, since time is good
						if line[sbs_fields["CS"]] != '':
							callsigns.update({icao:line[sbs_fields["CS"]].strip()})

						if line[sbs_fields["Alt"]] != '':
							altitudes.update({icao:int(line[sbs_fields["Alt"]])})

						if line[sbs_fields["GS"]] != '':
							speeds.update({icao:int(line[sbs_fields["GS"]])})

						if line[sbs_fields["Trk"]] != '':
							headings.update({icao:int(line[sbs_fields["Trk"]])})

						if line[sbs_fields["Lat"]] != '':
							latitudes.update({icao:float(line[sbs_fields["Lat"]])})

						if line[sbs_fields["Lng"]] != '':
							longitudes.update({icao:float(line[sbs_fields["Lng"]])})

						if line[sbs_fields["VR"]] != '':
							vert_rate.update({icao:int(line[sbs_fields["VR"]])})

						if line[sbs_fields["Sq"]] != '':
							squarks.update({icao:line[sbs_fields["Sq"]].strip()})

						if line[sbs_fields["Gnd"]] != '':
							ongrounds.update({icao:line[sbs_fields["Gnd"]].strip()})


						if line[sbs_fields["TT"]] == "3":
							# Check to see if it is time for an update. Either no report, or time has elapsed
							if (not icao in updates):
								do_update = True
							else:
								if timestamp > updates[icao]: # Sometimes had timestamp repeat, so had 0 time diff
									timedifference = timestamp - updates[icao]
									try:
										if speeds[icao] > 0:
											est_time_diff = UPDATE_DIST / (speeds[icao] * 0.514444)
											if timedifference.total_seconds() >= est_time_diff:
												do_update = True
											else:
												do_update = False
										else:
											do_update = False
									except Exception as e:
										print(f"{line_timestamp} Calculation of time diff error: {e} -- {d}")

							# Do an update to SQL when all the data is available & either 1st time or time has elapsed
							try:
								if do_update and icao in callsigns and icao in altitudes and icao in speeds and icao in headings and \
								   icao in latitudes and icao in longitudes and icao in vert_rate:

									sSQL = f"INSERT INTO aircraft_details VALUES ('{sql_ts}', '{icao}', '{callsigns[icao]}', {latitudes[icao]}, {longitudes[icao]}, {altitudes[icao]}, {speeds[icao]}, {headings[icao]}, {vert_rate[icao]})"

									# Calculate relative position
									aircraft_pos = (latitudes[icao], longitudes[icao])
									horiz_dist = haversine(my_pos, aircraft_pos, unit='km') # Work in km
									vert_dist = (float(altitudes[icao]) - my_alt) * 0.0003048 # Work in km
									total_dist = math.sqrt(horiz_dist*horiz_dist + vert_dist*vert_dist)

									print(f"{sql_ts} {icao} {callsigns[icao]} {latitudes[icao]}° {longitudes[icao]}° {speeds[icao]}kt {altitudes[icao]}ft {vert_rate[icao]}ft/min -- %.1fkm" % total_dist)

									cur.execute(sSQL)
									updates.update({icao:timestamp})
							except Exception as e:
								print(f"{line_timestamp} Store update error: {e} -- {d}\n")

							# Store ALL data into the InfluxDB if it is available
							if influx_valid and icao in callsigns and icao in altitudes and icao in speeds and icao in headings and \
								   icao in latitudes and icao in longitudes and icao in vert_rate and icao in squarks:
								try:
									# influx_ts = datetime.datetime.strftime(timestamp, "%Y-%m-%dT%H:%M:%S.%fZ")
									influxpoint = Point("adsb_obs") \
									  .tag("icao_id", icao) \
									  .tag("callsign", callsigns[icao]) \
									  .field("latitude", latitudes[icao]) \
									  .field("longitude", longitudes[icao]) \
									  .field("altitude", altitudes[icao]) \
									  .field("speed", speeds[icao]) \
									  .field("vert_rate", vert_rate[icao]) \
									  .field("heading", headings[icao]) \
									  .field("squark", squarks[icao]) \
									  .time(timestamp, WritePrecision.NS)

									influx_write_api.write(influx_config["BUCKET"], influx_config["ORG"], influxpoint)
								except Exception as e:
									print(f"\n\n{line_timestamp} InfluxDB error: {e}")
									print(f"{d}\n{icao} {callsigns[icao]} float:{latitudes[icao]} float:{longitudes[icao]} int:{altitudes[icao]} int:{speeds[icao]} int:{vert_rate[icao]} int:{headings[icao]}\n\n")

						# since everything was valid we reset the stream message
						data_str = ""
				else:
					# the stream message is too short, prepend to the next stream message
					data_str = d
					continue

	except KeyboardInterrupt:
		print("\nClosing connection" )
		s.close()

		conn.commit()
		conn.close()

		if influx_valid:
			influxclient.close()		


	except Exception as e:
		print(f"General exception: {e}")
		print("Error with ", line)
		quit()

def connect_to_socket(loc,port):
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect((loc, port))
	return s

if __name__ == '__main__':
	main()

